BINNAME=refactor
PRETTIFY=prettify
RELEASE=-s -w

GOOS=$(shell uname -s | tr [A-Z] [a-z])
GOARGS=GOARCH=amd64 CGO_ENABLED=1
ifeq ($(GOOS),windows)
    GOBIN=go
    UPXBIN=
else
    ifeq ($(GOOS),darwin)
        GOBIN=/usr/local/bin/go
        UPXBIN=/usr/local/bin/upx
    else
        GOBIN=/usr/bin/go
        UPXBIN=/usr/bin/upx
    endif
endif
GOBUILD=$(GOARGS) $(GOBIN) build -ldflags="$(RELEASE)"

.PHONY: all build clean upx

all: clean build
build:
	@echo "Compile $(BINNAME) $(PRETTIFY) ..."
	GOOS=$(GOOS) $(GOBUILD) -o $(BINNAME) ./cmd/reverse/
	GOOS=$(GOOS) $(GOBUILD) -o $(PRETTIFY) ./cmd/prettify/
	@echo "Build success."
clean:
	rm -f $(BINNAME) $(PRETTIFY)
	@echo "Clean all."
upx: clean build
	$(UPXBIN) $(BINNAME) $(PRETTIFY)
