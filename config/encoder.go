package config

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"strings"

	"gitee.com/azhai/xorm-refactor/v2/utils"
	"github.com/BurntSushi/toml"
	"gopkg.in/yaml.v3"
)

type MarshalFunc func(in interface{}) (out []byte, err error)
type UnmarshalFunc func(in []byte, out interface{}) (err error)

func UnmarshalFile(unmarshal UnmarshalFunc, data interface{}, fileName string) error {
	content, err := ioutil.ReadFile(fileName)
	if err == nil {
		err = unmarshal(content, data)
	}
	return err
}

func MarshalFile(marshal MarshalFunc, data interface{}, fileName string) error {
	content, err := marshal(data)
	if err == nil {
		err = ioutil.WriteFile(fileName, content, utils.DEFAULT_FILE_MODE)
	}
	return err
}

// GetExtName 获取文件扩展名
func GetExtName(fileName string) string {
	pos := strings.LastIndex(fileName, ".")
	if pos > 0 {
		return strings.ToLower(fileName[pos+1:])
	} else {
		return ""
	}
}

// LoadDataFile 加载配置文件
func LoadDataFile(data interface{}, fileName string) error {
	var unmarshal UnmarshalFunc
	switch GetExtName(fileName) {
	case "json":
		unmarshal = json.Unmarshal
	case "toml":
		unmarshal = toml.Unmarshal
	default:
		unmarshal = yaml.Unmarshal
	}
	return UnmarshalFile(unmarshal, data, fileName)
}

// SaveDataFile 保存配置文件
func SaveDataFile(data interface{}, fileName string) error {
	var marshal MarshalFunc
	switch GetExtName(fileName) {
	case "json":
		marshal = func(in interface{}) (out []byte, err error) {
			return json.MarshalIndent(in, "", strings.Repeat(" ", 4))
		}
	case "toml":
		marshal = func(in interface{}) (out []byte, err error) {
			buf := new(bytes.Buffer)
			err = toml.NewEncoder(buf).Encode(in)
			return buf.Bytes(), err
		}
	default:
		marshal = yaml.Marshal
	}
	return MarshalFile(marshal, data, fileName)
}
