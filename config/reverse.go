// Copyright 2019 The Xorm Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package config

import (
	"fmt"
	"path/filepath"

	"gitee.com/azhai/xorm-refactor/v2/config/dialect"
	"gitee.com/azhai/xorm-refactor/v2/utils"
	"github.com/gomodule/redigo/redis"

	//_ "github.com/mattn/go-oci8"
	_ "github.com/denisenkom/go-mssqldb"
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
	_ "github.com/mattn/go-sqlite3"
	"xorm.io/xorm"
)

const ( // 约定大于配置
	INIT_FILE_NAME   = "init"
	CONN_FILE_NAME   = "conn"
	SINGLE_FILE_NAME = "models"
	QUERY_FILE_NAME  = "queries"
)

// ReverseSource represents a reverse source which should be a database connection
type ReverseSource struct {
	DriverName   string             `json:"driver_name" yaml:"driver_name" toml:"driver_name"`
	TablePrefix  string             `json:"table_prefix,omitempty" yaml:"table_prefix,omitempty" toml:"table_prefix"`
	ImporterPath string             `json:"importer_path,omitempty" yaml:"importer_path,omitempty" toml:"importer_path"`
	ConnStr      string             `json:"conn_str" yaml:"conn_str" toml:"conn_str"`
	OptStr       string             `json:"opt_str,omitempty" yaml:"opt_str,omitempty" toml:"opt_str"`
	options      []redis.DialOption `json:"-" yaml:"-" toml:"-"`
}

func NewReverseSource(c ConnConfig) (*ReverseSource, dialect.Dialect) {
	d := dialect.GetDialectByName(c.DriverName)
	r := &ReverseSource{
		DriverName:   c.DriverName,
		TablePrefix:  c.TablePrefix,
		ConnStr:      d.ParseDSN(c.Params),
		ImporterPath: d.ImporterPath(),
	}
	if dr, ok := d.(*dialect.Redis); ok {
		r.options = dr.GetOptions()
		r.OptStr = dr.Values.Encode()
	}
	return r, d
}

func (r ReverseSource) Connect(verbose bool) (*xorm.Engine, redis.Conn, error) {
	if r.DriverName == "" || r.ConnStr == "" {
		err := fmt.Errorf("the config of connection is empty")
		return nil, nil, err
	} else if verbose {
		fmt.Println("Connect:", r.DriverName, r.ConnStr)
	}
	if r.DriverName == "redis" {
		conn, err := redis.Dial("tcp", r.ConnStr, r.options...)
		return nil, conn, err
	}
	engine, err := xorm.NewEngine(r.DriverName, r.ConnStr)
	if err == nil {
		engine.ShowSQL(verbose)
	}
	return engine, nil, err
}

// ReverseTarget represents a reverse target
type ReverseTarget struct {
	Language          string   `json:"language" yaml:"language" toml:"language"`
	IncludeTables     []string `json:"include_tables" yaml:"include_tables" toml:"include_tables"`
	ExcludeTables     []string `json:"exclude_tables" yaml:"exclude_tables" toml:"exclude_tables"`
	InitNameSpace     string   `json:"init_name_space" yaml:"init_name_space" toml:"init_name_space"`
	OutputDir         string   `json:"output_dir" yaml:"output_dir" toml:"output_dir"`
	TemplatePath      string   `json:"template_path" yaml:"template_path" toml:"template_path"`
	QueryTemplatePath string   `json:"query_template_path,omitempty" yaml:"query_template_path,omitempty" toml:"query_template_path"`
	InitTemplatePath  string   `json:"init_template_path,omitempty" yaml:"init_template_path,omitempty" toml:"init_template_path"`

	TableMapper  string            `json:"table_mapper,omitempty" yaml:"table_mapper,omitempty" toml:"table_mapper"`
	ColumnMapper string            `json:"column_mapper,omitempty" yaml:"column_mapper,omitempty" toml:"column_mapper"`
	Funcs        map[string]string `json:"funcs,omitempty" yaml:"funcs,omitempty" toml:"funcs"`
	Formatter    string            `json:"formatter,omitempty" yaml:"formatter,omitempty,omitempty" toml:"formatter"`
	Importter    string            `json:"importter,omitempty" yaml:"importter,omitempty,omitempty" toml:"importter"`
	ExtName      string            `json:"-" yaml:"-" toml:"-"`
	NameSpace    string            `json:"-" yaml:"-" toml:"-"`

	MultipleFiles  bool   `json:"multiple_files" yaml:"multiple_files" toml:"multiple_files"`
	ApplyMixins    bool   `json:"apply_mixins" yaml:"apply_mixins" toml:"apply_mixins"`
	MixinDirPath   string `json:"mixin_dir_path,omitempty" yaml:"mixin_dir_path,omitempty" toml:"mixin_dir_path"`
	MixinNameSpace string `json:"mixin_name_space,omitempty" yaml:"mixin_name_space,omitempty" toml:"mixin_name_space"`
}

func DefaultReverseTarget(nameSpace string) ReverseTarget {
	return ReverseTarget{
		Language:      "golang",
		InitNameSpace: nameSpace + "/models",
		OutputDir:     "./models",
	}
}

func DefaultMixinReverseTarget(nameSpace string) ReverseTarget {
	rt := DefaultReverseTarget(nameSpace)
	rt.ApplyMixins = true
	rt.MixinDirPath = filepath.Join(rt.OutputDir, "mixins")
	rt.MixinNameSpace = rt.InitNameSpace + "/mixins"
	return rt
}

func (t ReverseTarget) GetParentOutDir(backward int) string {
	outDir := t.OutputDir
	for i := 0; i < backward; i++ {
		outDir = filepath.Dir(outDir)
	}
	return outDir
}

func (t ReverseTarget) GetFileName(dir, name string) string {
	fileName := filepath.Join(dir, name+t.ExtName)
	utils.MkdirForFile(fileName, true)
	return fileName
}

func (t ReverseTarget) GetOutFileName(name string) string {
	return t.GetFileName(t.OutputDir, name)
}

func (t ReverseTarget) GetParentOutFileName(name string, backward int) string {
	outDir := t.GetParentOutDir(backward)
	return t.GetFileName(outDir, name)
}

func (t ReverseTarget) RegularizeOptions(key string, src *ReverseSource) ReverseTarget {
	if t.Language == "" {
		t.Language = "golang"
	}
	return t
}
