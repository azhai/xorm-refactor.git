package refactor_test

import (
	"strings"
	"testing"

	refactor "gitee.com/azhai/xorm-refactor/v2"
	"gitee.com/azhai/xorm-refactor/v2/config"
	"gitee.com/azhai/xorm-refactor/v2/rewrite"
	"gitee.com/azhai/xorm-refactor/v2/utils"
	"github.com/stretchr/testify/assert"
)

var (
	fileName     = "settings.yml"
	saveFileName = "settings.json"
	nameSpace    = "gitee.com/azhai/xorm-refactor/v2/models"
)

// go test -run=Configure
func Test_01_Configure(t *testing.T) {
	cfg, err := config.ReadSettings(fileName, nameSpace)
	assert.NoError(t, err)
	assert.Equal(t, "golang", cfg.ReverseTarget.Language)
	err = config.SaveDataFile(cfg, saveFileName)
	assert.NoError(t, err)
}

// go test -run=Reverse
func Test_02_Reverse(t *testing.T) {
	verbose := testing.Verbose()
	settings, err := config.ReadSettings(fileName, nameSpace)
	settings.ReverseTarget.ApplyMixins = false
	err = refactor.ExecReverseSettings(settings, verbose)
	assert.NoError(t, err)
}

// go test -run=Mixin
func Test_03_Mixin(t *testing.T) {
	verbose := testing.Verbose()
	settings, err := config.ReadSettings(fileName, nameSpace)
	err = refactor.ExecApplyMixins(&settings.ReverseTarget, verbose)
	assert.NoError(t, err)
}

// go test -run=Prettify
func Test_04_Prettify(t *testing.T) {
	files, err := utils.FindFiles("./", ".go")
	assert.NoError(t, err)
	for f := range files {
		if strings.Contains(f, "vendor/") {
			continue
		}
		t.Log(f)
		_, err = rewrite.PrettifyGolangFile(f, true)
	}
	assert.NoError(t, err)
}
