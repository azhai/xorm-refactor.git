package rewrite

import (
	"bytes"
	"go/format"
	"io/ioutil"

	"gitee.com/azhai/xorm-refactor/v2/utils"
	"golang.org/x/tools/imports"
)

// FormatGolangCode 格式化代码，如果出错返回原内容
func FormatGolangCode(src []byte) ([]byte, error) {
	_src, err := format.Source(src)
	if err == nil {
		src = _src
	}
	return src, err
}

func WriteCodeFile(fileName string, codeText []byte) ([]byte, error) {
	err := ioutil.WriteFile(fileName, codeText, utils.DEFAULT_FILE_MODE)
	return codeText, err
}

func writeGolangFilePrettify(fileName string, codeText []byte, cleanImports bool) ([]byte, error) {
	// Formart/Prettify the code 格式化代码
	srcCode, err := FormatGolangCode(codeText)
	if err != nil {
		//fmt.Println(err)
		//fmt.Println(string(codeText))
		return codeText, err
	}
	if cleanImports { // 清理 import
		cs := NewCodeSource()
		cs.SetSource(srcCode)
		if cs.CleanImports() > 0 {
			if srcCode, err = cs.GetContent(); err != nil {
				return srcCode, err
			}
		}
	}
	if _, err = WriteCodeFile(fileName, srcCode); err != nil {
		return srcCode, err
	}
	// Split the imports in two groups: go standard and the third parts 分组排序引用包
	var dstCode []byte
	dstCode, err = imports.Process(fileName, srcCode, nil)
	if err != nil {
		return srcCode, err
	}
	return WriteCodeFile(fileName, dstCode)
}

func WriteGolangFile(fileName string, codeText []byte) ([]byte, error) {
	return writeGolangFilePrettify(fileName, codeText, false)
}

func CleanImportsWriteGolangFile(fileName string, codeText []byte) ([]byte, error) {
	return writeGolangFilePrettify(fileName, codeText, true)
}

// PrettifyGolangFile 格式化Go文件
func PrettifyGolangFile(fileName string, cleanImports bool) (changed bool, err error) {
	var srcCode, dstCode []byte
	if srcCode, err = ioutil.ReadFile(fileName); err != nil {
		return
	}
	dstCode, err = writeGolangFilePrettify(fileName, srcCode, cleanImports)
	if bytes.Compare(srcCode, dstCode) != 0 {
		changed = true
	}
	return
}

// RewritePackage 将包中的Go文件格式化，如果提供了pkgname则用作新包名
func RewritePackage(pkgpath, pkgname string) error {
	if pkgname != "" {
		// TODO: 替换包名
	}
	files, err := utils.FindFiles(pkgpath, ".go")
	if err != nil {
		return err
	}
	var content []byte
	for fileName := range files {
		content, err = ioutil.ReadFile(fileName)
		if err != nil {
			break
		}
		_, err = WriteGolangFile(fileName, content)
		if err != nil {
			break
		}
	}
	return err
}
