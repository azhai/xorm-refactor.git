package utils

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

// 连接配置
type ConnParams struct {
	Host     string                 `json:"host" yaml:"host" toml:"host"`
	Port     int                    `json:"port,omitempty" yaml:"port,omitempty" toml:"port"`
	Username string                 `yaml/json:"username,omitempty" toml:"username"`
	Password string                 `toml/yaml/json:"password"`
	Database string                 `toml/yaml/json:"database"`
	Options  map[string]interface{} `yaml/json:"options,omitempty" toml:"options"`
}

func getStructTags(v interface{}) (tags map[string]reflect.StructTag) {
	tags = make(map[string]reflect.StructTag)
	vt := GetIndirectType(v)
	for i := 0; i < vt.NumField(); i++ {
		field := vt.Field(i)
		tags[field.Name] = field.Tag
	}
	return
}

// go test -run=Burnish
func Test_01_Itag_Burnish(t *testing.T) {
	tags := getStructTags(ConnParams{})
	tag := NewItag()
	tag.Burnish(tags["Port"], "json", "toml", "yaml")
	assert.Equal(t, "port,omitempty", tag.Get("yaml"))
	assert.Equal(t, `json:"port,omitempty" toml:"port" yaml:"port,omitempty"`, tag.String())
}

// go test -run=Parse
func Test_02_Itag_Parse(t *testing.T) {
	tags := getStructTags(ConnParams{})
	tag := NewItag()
	tag.Parse(tags["Username"])
	assert.Equal(t, "username,omitempty", tag.Get("yaml"))
	assert.Equal(t, `json:"username,omitempty" toml:"username" yaml:"username,omitempty"`, tag.String())
	tag = NewItag()
	tag.Parse(tags["Password"])
	assert.Equal(t, "password", tag.Get("yaml"))
	assert.Equal(t, `json:"password" toml:"password" yaml:"password"`, tag.String())
}
